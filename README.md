## Описание:

   В данном проекте реализована авторизация и регистрация через Facebook, а также возможность для авторизованных 
   пользователей оставлять свои сообщения, комментировать сообщения и отвечать на комментарии. Комментарии реализованы
   в виде древовидной структуры. Авторизованные пользователи также имеют возможность редактировать и удалять свои сообщения

## Установка:

        php composer.phar install -o
   
   *Примечание: для запуска приложения должны быть указаны корректные параметры подключения к базе данных и
                facebook.api (параметры: facebook_client_id, facebook_client_secret).
                Параметр  mailer_user не должен быть null и должен быть наример, example@test.com.

## Структура папок:

   migrations/*    - дамп базы данных
 
   app/*           - конфигурационные файлы и файлы представлений

   src/*           - файлы проекта
   
   web/*           - файлы шрифтов, css, js и картинки проекта доступные для браузера

   features/*      - Тесты behat


## Настройка:

1) База данных:
   Тестовая база данных представлена в директории migrations/.
   Для создания базы данных и таблиц de novo в директории проекта в консоли введите комманды:
   
        $ php bin/console doctrine:database:create 

        $ php bin/console doctrine:schema:update --force 
   
2) Тестирование:
   В проекте представлены два сценария, описанные с помощью Behat: 
      - authorized_user 
      - not_authorized_user
   Для запуска сценариев в директории проекта в консоли введите:
   - Для ОС Windows:

        $ php vendor/behat/behat/bin/behat --suite=default features/ИМЯ_СЦЕНАРИЯ.feature 
   
   - Для ОС Linux:

        $ vendor/bin/behat --suite=default features/ИМЯ_СЦЕНАРИЯ.feature
   
       либо

        $ bin/behat --suite=default features/ИМЯ_СЦЕНАРИЯ.feature 

   Для корректной работы сценария authorized_user.feature в базе данных должен присутствовать 
   тестовый пользователь с username - 'test'.
   
   Для создания тестового пользователя в директории проекта в консоли введите:
   
        $ php bin/console fos:user:create test test@example.com password 

