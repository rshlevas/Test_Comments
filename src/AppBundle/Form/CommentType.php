<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Class that could be used for building form 
 * for comment creating
 *
 * @category AbstractTypeClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class CommentType extends AbstractType
{
    /**
     * Function that builds form
     * 
     * @param FormBuilderInterface $builder 
     * @param array                $options Array with options (could be empty)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, ['label' => 'Текст комментария']) 
            ->add('save', SubmitType::class, ['label' => 'Опубликовать'])
        ;
    }
}