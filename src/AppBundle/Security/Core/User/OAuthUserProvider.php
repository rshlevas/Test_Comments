<?php
namespace AppBundle\Security\Core\User;
 
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class OAuthUserProvider
 * @package AppBundle\Security\Core\User
 */
class OAuthUserProvider extends BaseClass
{       
    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $socialID = $response->getUsername();
        $user = $this->userManager->findUserBy([$this->getProperty($response)=>$socialID]);
        $email = $response->getEmail();
        $no_email = "facebook_$socialID@test.com";
        $fullName = $response->getRealName();
        $username = "fb_user_$socialID";
        //check if the user already has the corresponding social account
        if (null === $user) {
            //check if the user has a normal account
            if ($email) {
                $user = $this->userManager->findUserByEmail($email);
            } else {
                $user = $this->userManager->findUserByEmail($no_email);
            }             
            if (null === $user || !$user instanceof UserInterface) {
                //if the user does not have a normal account, set it up:
                $user = $this->userManager->createUser();
                if ($email) {
                    $user->setEmail($email);
                    $user->setEmailCanonical($email);
                } else {
                    $user->setEmail($no_email);
                    $user->setEmailCanonical($no_email);
                }
                $user->setUsername($username);
                $user->setUsernameCanonical($username);
                $user->setFullName($fullName);
                $user->setPlainPassword(md5(uniqid()));
                $user->setEnabled(true);
            }
            //then set its corresponding social id
            $user->setFacebookID($socialID);
            $this->userManager->updateUser($user);
        } else {
            //and then login the user
            $checker = new UserChecker();
            $checker->checkPreAuth($user);
        }
 
        return $user;
    }
}