<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Storage of SystemMessage object.
 * 
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * Id of the user
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Entity/Comment objects related to this user
     * 
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     */
    private $comments;
    
    /**
     * @var string Fullname of the user
     * 
     * @ORM\Column(name="full_name", type="string", nullable=true)
     */
    protected $fullName;

    /**
     * @var string Facebook id of the user
     *
     * @ORM\Column(name="facebook_id", type="string", nullable=true)
     */
    private $facebookID;
   
    public function __construct()
    {
        parent::__construct();
        $this->comments = new ArrayCollection();
    }
    
    /**
     * Returns comments related to this user
     * 
     * @return ArrayCollection
     */
    public function getMessages() 
    {
        return $this->messages;
    }
    
    /**
     * Function that set facebook id of the user
     * 
     * @param string $facebookID
     * @return User
     */
    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;

        return $this;
    }

    /**
     * Returns facebook id of the user
     * 
     * @return string
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }
    
    /**
     * Function, that set fullname of user from 
     * Facebook account
     * 
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Function, that returns fullname of user from 
     * Facebook account
     * 
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }
    
    /**
     * Function that return string value of the class
     * 
     * @return string
     */
    public function __toString() 
    {
        return (string) $this->getFullName();
    }
}