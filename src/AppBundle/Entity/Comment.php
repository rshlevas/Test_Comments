<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Storage of Comment object.
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 * @ORM\Table(name="comments")
 */
class Comment
{
    /**
     * Id of the comment
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Id of the User related to this comment
     * 
     * @ORM\Column(type="integer")
     */
    private $user_id;
    
    /**
     * Id of the parent comment 
     * 
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parent_id;
    
    /**
     * Content of the comment
     * 
     * @ORM\Column(type="text")
     */
    private $content;
    
    /**
     * Date when message was sent
     * 
     * @ORM\Column(type="datetime")
     */
    private $date;
    
    /**
     * Entity/User object related to this message object
     * 
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * Entity/Comment object related to this comment object
     * 
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    
    /**
     * Entity/Comment objects related to this comment object
     * 
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent")
     */
    private $children;
    
    /**
     * Intial function
     * 
     * @return void
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }
    
    /**
     * Returns id of the object
     * 
     * @return integer
     */
    
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set the if of the object
     * 
     * @param  integer $id <p>Id of the object</p>
     * @return integer
     */
    public function setId($id)
    {
        return $this->id = $id;
    }
    
    /**
     * Returns id of the user that related to this object
     * 
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     * Sets id of the user that related to this object
     * 
     * @param  integer $user_id <p>Id of the user that related to this object</p>
     * @return integer
     */
    public function setUserId($user_id)
    {
        return $this->user_id = $user_id;
    }
    
    /**
     * Returns id of the comment that related to this object
     * 
     * @return integer
     */
    public function getParentId()
    {
        return $this->parent_id;
    }
    
    /**
     * Sets id of the comment that related to this object
     * 
     * @param  integer $parent_id <p>Id of the comment that related to this object</p>
     * @return integer
     */
    public function setParentId($parent_id)
    {
        return $this->parent_id = $parent_id;
    }
    
    /**
     * Returns content of message
     * 
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets content of message
     * 
     * @param  string $content <p>Content of the message</p>
     * @return string
     */     
    public function setContent($content)
    {
        return $this->content = $content;
    }
    
    /**
     * Returns date of message sending
     * 
     * @return \DateTime object
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Sets date of message sending
     * 
     * @param  \DateTime $dueDate <p>Date when message was sent</p>
     * @return \DateTime 
     */
    public function setDate(\DateTime $dueDate = null)
    {
        return $this->date = $dueDate;
    }
    
    /**
     * Returns comment related to this comment
     * 
     * @param  \Entity\Comment $parent <p>Parent comment related to this message</p>
     * @return \Entity\Comment 
     */
    public function setParent(Comment $parent) 
    {
        return $this->parent = $parent;
    }
    
    /**
     * Returns parent comment related to this comment
     * 
     * @return \Entity\Comment object
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Returns user related to this message
     * 
     * @return \Entity\User object
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Returns user related to this message
     * 
     * @param  \Entity\User $user <p>User related to this message</p>
     * @return \Entity\User 
     */
    public function setUser(User $user) 
    {
        return $this->user = $user;
    }
    
    /**
     * Function that return string value of the class
     * 
     * @return string
     */
    public function __toString() 
    {
        return (string) $this->content;
    }
    
    /**
     * Returns child comment related to this user
     * 
     * @return ArrayCollection
     */
    public function getChildren() 
    {
        return $this->children;
    }
}