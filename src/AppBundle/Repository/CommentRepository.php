<?php
// src/AppBundle/Repository/CommentRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class related to Repository category and used for 
 * work with Entity/Comment DB repository
 *
 * @category Repository
 * @package   
 * @author    
 * @license  
 * @link     
 */
class CommentRepository extends EntityRepository
{
    /**
     * Funtion that return the array of Comment objects
     * that are parent comments
     * 
     * @return type array <p>Array with comment objects</p>
     */
    public function findAllParentComments()
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT c FROM AppBundle:Comment c
            WHERE c.parent_id is NULL ORDER BY c.date DESC'
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    /**
     * Fucntion that look for comment object by its content
     * 
     * @param  string $content  <p>Content string</p>
     * @return array            <p>with a comment object<p>
     */
    public function findCommentByContent($content)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT c FROM AppBundle:Comment c
            WHERE c.content = :content'
        )->setParameter('content', $content);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }        
    }
}

