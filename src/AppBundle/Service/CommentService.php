<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Comment;
use AppBundle\Entity\User;

/**
 * Class that used to work with comments
 *
 * @category ServiceClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class CommentService
{

    /**
     * Object of EntityManagerInterface
     */
    private $em;
    
    /**
     * Initial function
     * 
     * @param  type $em 
     * @return void
     */
    public function __construct(EntityManagerInterface $em) 
    {
        $this->em = $em;
    }
    
    /**
     * Function that creates comment and push it into DB
     * 
     * @param  object $comment  <p>Comment with content</p>     
     * @param  object $user     <p>Author of the comment</p>
     * @param  object $parent   <p>Parent comment related to $comment</p>
     */
    public function createComment(Comment $comment, User $user, Comment $parent = null)
    {
        $comment->setDate(new \DateTime);
        $comment->setUserId($user->getId());
        $comment->setUser($user);
        if ($parent) {
            $comment->setParentId($parent->getId());
            $comment->setParent($parent);
        }
        $this->em->persist($comment);
        $this->em->flush();
        return true;
    }
    
    /**
     * Function that update comment and push it into DB
     * 
     * @param  object $comment  <p>Comment with content</p>  
     * @return boolean true   
     */
    public function updateComment(Comment $comment)
    {
        $this->em->flush();
        return true;
    }
    
    /**
     * Function that delete comment from DB
     * 
     * @param  object $comment  <p>Comment with content</p>     
     * @return boolean true  
     */
    public function deleteComment(Comment $comment)
    {   
        $this->em->remove($comment);
        $this->em->flush();
        return true;
    }
    
    /**
     * Function that find comment by content from DB
     * 
     * @param  string $content  <p>Content of the comment</p>
     * @return object Entity/Comment     
     */
    public function getCommentByContent($content)
    {
        return $this->em->getRepository('AppBundle:Comment')
            ->findCommentByContent($content);
    }        
}


