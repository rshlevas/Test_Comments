<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use AppBundle\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use AppBundle\Form\ReplyType;
use AppBundle\Form\EditType;

/**
 * Class related to Controller category and used for AJAX
 * requests for routes with /comment prefix
 *
 * @category Controller
 * @package   
 * @author    
 * @license  
 * @link     
 */
class CommentController extends Controller
{
    /** 
     * @Route(
     *     "/comment/reply/{parent}", 
     *     name="comment_reply", 
     *     requirements={"parent": "\d+"}
     * )
     */
    public function replyAction(Comment $parent, Request $request)
    {
        $comment = new Comment();
        $user = $this->getUser();
        $form = $this->createForm(ReplyType::class, $comment);
        $status = "";
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $commentService = $this->get('comment_service');
            if ($commentService->createComment($comment, $user, $parent)) {
                $status = "Sucsess";
            } else {
                $status = "invalid";
            }   
            return new \Symfony\Component\HttpFoundation\JsonResponse(["status" => $status]);
        }
        $html = $this->renderView(
            'form/comment_form.html.twig', 
            [
                'form' => $form->createView(),
            ]
        );
        return new \Symfony\Component\HttpFoundation\JsonResponse(["html" => $html]);
    }
    
    /** 
     * @Route(
     *     "/comment/edit/{comment}", 
     *     name="comment_edit", 
     *     requirements={"comment": "\d+"}
     * )
     */
    public function editAction(Comment $comment, Request $request)
    {
        $form = $this->createForm(EditType::class, $comment);
        $status = "";
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $commentService = $this->get('comment_service');
            if ($commentService->updateComment($comment)) {
                $status = "Sucsess";
            } else {
                $status = "invalid";
            }   
            return new \Symfony\Component\HttpFoundation\JsonResponse(["status" => $status]);
        }
        $html = $this->renderView(
            'form/comment_form.html.twig', 
            [
                'form' => $form->createView(),
            ]
        );
        return new \Symfony\Component\HttpFoundation\JsonResponse(["html" => $html]);
    }
    
    /** 
     * @Route(
     *     "/comment/delete/{comment}", 
     *     name="comment_delete", 
     *     requirements={"comment": "\d+"}
     * )
     */
    public function deleteAction(Comment $comment)
    {
        $commentService = $this->get('comment_service');
        $commentService->deleteComment($comment);
        $html = $this->renderView('comment/comment_delete_message.html.twig');
        return new \Symfony\Component\HttpFoundation\JsonResponse(["html" => $html]);
    }
}