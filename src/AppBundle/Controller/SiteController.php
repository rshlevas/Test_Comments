<?php

// src/AppBundle/Controller/SiteController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use AppBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class related to Controller category and used for homepage route
 *
 * @category Controller
 * @package   
 * @author    
 * @license  
 * @link     
 */
class SiteController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(EntityManagerInterface $em, Request $request)
    {
        $comments = $em->getRepository('AppBundle:Comment')
                ->findAllParentComments();
        $comment = new Comment();
        $user = $this->getUser();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $commentService = $this->get('comment_service');
            $commentService->createComment($comment, $user);
            return $this->redirectToRoute('homepage');
        }
        return $this->render(
            'main/comments.html.twig', 
            [
                'comments' => $comments,
                'form' => $form->createView(),
                'user' => $user,
            ]
        );
    }
    
    
}
