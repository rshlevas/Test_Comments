Feature: Authorized user on the main page

Scenario: Checking of ability of authorized user left comments
    Given I am authenticated as "test"
    And I am on "/"
    Then I should not see "Для публикации комментариев Вам необходимо выполнить авторизацию"
    Then I should see "Текст комментария"
    Then I should not see "Hello, I'm testing!"
    When I fill in "comment_content" with "Hello, I'm testing!"
    When I press "comment_save"
    Then I should be on "/"
    Then I should see "Hello, I'm testing!"
    Then I delete comment with message "Hello, I'm testing!"
    