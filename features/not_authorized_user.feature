Feature: Non authorized user on the main page

Scenario: Checking of ability of non authorized user left comments
    Given I am on "/"
    Then I should see "Для публикации комментариев Вам необходимо выполнить авторизацию"
    Then I should not see "Текст комментария"
    Then I should not see "Опубликовать"
    Then I should not see "Ответить"
    When I follow "авторизацию"
    Then I should be on "/login"

    